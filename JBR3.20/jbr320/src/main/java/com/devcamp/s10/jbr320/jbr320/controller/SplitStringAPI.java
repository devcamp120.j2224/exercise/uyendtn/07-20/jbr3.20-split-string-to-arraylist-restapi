package com.devcamp.s10.jbr320.jbr320.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class SplitStringAPI {
    @CrossOrigin
    @GetMapping("split")
    public ArrayList<String> splitString() {
       String inputString = "red,orange,blue,yellow,green";
       String[] color = inputString.split(",");
        ArrayList<String> splitResult = new ArrayList<String>();
        for (int i = 0; i < color.length; i++){
            splitResult.add(color[i]);
        }
        return splitResult;
    }
}
